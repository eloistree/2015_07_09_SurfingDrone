﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TrackedKinectPointsBody : MonoBehaviour {

    public GameObject jointPrefab;
    public Transform playerRoot;
    public bool flip;
    public bool unityConversion = true;
    public KinectInterop.JointType[] joints;
    public enum TrackedScriptAt { This, Root, Joint }
    public TrackedScriptAt scriptPosition=TrackedScriptAt.Joint;
    private List<KinectInterop.JointType> jointsAdd = new List<KinectInterop.JointType>();
    private Dictionary<KinectInterop.JointType, TrackedPoint> jointsTracked = new Dictionary<KinectInterop.JointType, TrackedPoint>();
    private Dictionary<KinectInterop.JointType, Transform> jointsCubeView_Local = new Dictionary<KinectInterop.JointType, Transform>();
    private Dictionary<KinectInterop.JointType, Transform> jointsCubeView_Global = new Dictionary<KinectInterop.JointType, Transform>();


    public void Start() {

        if (jointPrefab == null) { jointPrefab = GameObject.CreatePrimitive(PrimitiveType.Sphere); jointPrefab.transform.localScale = 0.1f * Vector3.one; }
        foreach (KinectInterop.JointType j in joints) 
        {
            if (jointsTracked.ContainsKey(j)) continue;

            GameObject localObj = GameObject.Instantiate(jointPrefab) as GameObject;
            GameObject globalObj = GameObject.Instantiate(jointPrefab) as GameObject;

            jointsAdd.Add(j);

            GameObject scriptAddTarget = this.gameObject;
            if (scriptPosition == TrackedScriptAt.Joint)
                scriptAddTarget = localObj;
            else if (scriptPosition == TrackedScriptAt.Root && scriptPosition != null)
                scriptAddTarget = playerRoot.gameObject;
            else scriptAddTarget = this.gameObject;
                
            TrackingPointKinectJoint tpk = scriptAddTarget.AddComponent<TrackingPointKinectJoint>() as TrackingPointKinectJoint;
            tpk.Set("KinectBody:" + j, j);

            tpk.flip = flip;
            tpk.unityConversion = unityConversion;

            jointsTracked.Add(j, tpk.TrackedPoint);
            globalObj.name = "" + j;
            jointsCubeView_Global.Add(j, globalObj.transform);

            localObj.name = "" + j;
            localObj.transform.parent = playerRoot;
            jointsCubeView_Local.Add(j, localObj.transform);
        }

    }

    void Update() {


        foreach (KinectInterop.JointType j in jointsAdd)
        {
            TrackedPoint tpk;
            if (jointsTracked.TryGetValue(j, out tpk)) {
                Transform localCube;
                Transform globalCube;
                jointsCubeView_Local.TryGetValue(j, out localCube);
                jointsCubeView_Global.TryGetValue(j, out globalCube);

                if (localCube != null) { 
                    localCube.localPosition = tpk.LocalPosition;
                    localCube.localRotation = tpk.LocalRotation;
                }
                if (globalCube != null)
                {
                    globalCube.position = tpk.Position;
                    globalCube.rotation = tpk.Rotation;
                }
            }

        }
    }
  

}
