﻿using UnityEngine;
using System.Collections;


/// <summary>
/// Give the possibility  to track the position and rotation of the player 
/// </summary>
public class TrackingPointKinectJoint : BasicUnityTrackingPoint {

    /// <summary>
    /// Which joint of the body is tracked
    /// </summary>
    public KinectInterop.JointType joint;
    public enum User : int { Primary = -1, One = 0, Two = 1, Three = 2, Four = 3, Five = 4, Six = 5, Seven = 6, Eight = 7 }
    
    /// <summary>
    /// Determine which player is going to be tracked.
    /// </summary>
    public User user = User.Primary;
    /// <summary>
    /// Inverse the position on Z axe and rotate orientation of 180° in aim to be on the same axe of unity (Body direction in same way of the Z axis)
    /// </summary>
    public bool unityConversion=true;

    /// <summary>
    /// ???
    /// </summary>
    public bool flip;

    private  Quaternion defaultRot = Quaternion.identity;
    private  Vector3 defaultPos = Vector3.zero;


    public override void DefineAccessorDelegate()
    {
        TrackedPoint.accessPosition = GetPosition;
        TrackedPoint.accessLocalPosition = GetLocalPosition;
        TrackedPoint.accessRotation = GetRotation;
        TrackedPoint.accessLocalRotation = GetLocalRotation;
        TrackedPoint.OptinalLinkedTransform = this.transform;
    }

    /// <summary>
    /// Give access to the rotation in kinect space based on the player orientation
    /// </summary>
    private Quaternion GetLocalRotation()
    {
        KinectManager kinect = KinectManager.Instance;
        if (kinect == null) return defaultRot;
        long userId = GetUser(ref kinect);
        //if (kinect.IsJointTracked(userId, (int)joint)) return defaultRot;
        //Quaternion rot = kinect.GetJointOrientation(userId, (int)joint, flip) * kinect.GetUserOrientation(userId, flip);
        Quaternion rot = kinect.GetUserOrientation(userId, flip) * kinect.GetJointOrientation(userId, (int)joint, flip);
        if (unityConversion) RotateAroundPoint(ref rot, Quaternion.Euler(0, 180, 0));
        return rot;
    }

    /// <summary>
    /// Give access to the rotation in kinect space
    /// </summary>
    private Quaternion GetRotation()
    {
        KinectManager kinect = KinectManager.Instance;
        if (kinect == null) return defaultRot;
        long userId = GetUser(ref kinect);
        //if (kinect.IsJointTracked(userId, (int)joint)) return defaultRot;
        Quaternion rot =kinect.GetJointOrientation(userId, (int)joint, flip);
        if (unityConversion) RotateAroundPoint(ref rot, Quaternion.Euler(0, 180, 0));
        return rot;

    }

    /// <summary>
    /// Give access to the joint position in kinect space and adjuster with the player orientation and the player position.
    /// Zero is the foot position determined by the kinect.
    /// </summary>
    private Vector3 GetLocalPosition()
    {
        KinectManager kinect = KinectManager.Instance;
        if (kinect == null) return Vector3.zero;
        long userId = GetUser(ref kinect);

        //if (kinect.IsJointTracked(userId, (int)joint)) return defaultPos;
        Vector3 playerPosition = kinect.GetUserPosition(userId);
        Quaternion playerRotation = kinect.GetUserOrientation(userId,flip);
        Vector3 joinPosition = kinect.GetJointPosition(userId, (int) joint);
        

        Vector3 pos = Quaternion.Inverse(playerRotation) * (joinPosition - playerPosition);
        if (unityConversion) {
            pos.z *= -1;
            playerPosition.z *= -1;
        }
        pos.y += playerPosition.y ;
        return pos  ;
    }

    /// <summary>
    /// Give access to the joint position in kinect space.
    /// Zero is the foot position determined by the kinect.
    /// </summary>
    private Vector3 GetPosition()
    {
        KinectManager kinect = KinectManager.Instance;
        if (kinect == null) return Vector3.zero;
        long userId = GetUser(ref kinect);
        //if (kinect.IsJointTracked(userId, (int)joint)) return defaultPos;
        Vector3 pos = kinect.GetJointPosition(userId, (int)joint);
        if (unityConversion) {
            pos.z *= -1;
        }
        return pos;
    }


    private long GetUser(ref KinectManager kinect)
    {
        if (kinect == null) return -1;
        if (User.Primary == user) return kinect.GetPrimaryUserID();
        else return kinect.GetUserIdByIndex( (int) user);
    }


    public static void RotateAroundPoint(ref Vector3 position, Vector3 pivot, Quaternion rotateOf)
    {
        position = rotateOf * (position - pivot) + pivot;
    }
    public static void RotateAroundPoint(ref Quaternion orientation, Quaternion rotateOf)
    {
        orientation = orientation * rotateOf;
    }


    public void Set(string idName, KinectInterop.JointType joint)
    {
        this.idName= idName;
        this.joint = joint;
    }

 
}
