﻿using System;
using UnityEngine;

[System.Serializable]
public class KinectUserSelection
{

    public enum UserPriority { Default = -2, Closest = -1, P1 = 0, P2 = 1, P3 = 2, P4 = 3, P5 = 4, P6 = 5, AtSpecificPosition = -3 }
    public UserPriority userPriority = UserPriority.Closest;

    /// <summary>
    ///  Z: depth (0m ->6m)
    ///  X: side (-x m left <->  + x m right)
    ///  Y: height of the spine
    /// </summary>
    public Vector3 specificPosition = new Vector3(0, 0.7f, 2f);
   

    public long GetUserId()
    {
        KinectManager km = KinectManager.Instance;
        return GetUserId(this.userPriority, ref km, this.specificPosition);
    }
    public long GetUserId(ref KinectManager kinectManager)
    {
        return GetUserId(this.userPriority, ref kinectManager, this.specificPosition);
    }
    public long GetUserId(ref KinectManager kinectManager, Vector3 specificPosition)
    {
        return GetUserId(this.userPriority, ref kinectManager, specificPosition);
    }
    public long GetUserId(UserPriority userPriority, ref KinectManager kinectManager, Vector3 specificPosition)
    {
        return GetUserIdWanted(userPriority, ref  kinectManager, specificPosition);

    }

    public static long GetUserIdWanted(UserPriority userPriority, ref KinectManager kinectManager, Vector3 specificPosition)
    {
        long id; int index;
        switch (userPriority)
        {
            case UserPriority.Default:
                return kinectManager.GetPrimaryUserID();
            case UserPriority.Closest:
                if (GetClosestPlayer(ref kinectManager, out id, out index, Vector3.zero))
                    return id;
                else break;
            case UserPriority.P1:
            case UserPriority.P2:
            case UserPriority.P3:
            case UserPriority.P4:
            case UserPriority.P5:
            case UserPriority.P6:
                return kinectManager.GetUserIdByIndex((int)userPriority);
            case UserPriority.AtSpecificPosition:
                if (GetClosestPlayer(ref kinectManager, out id, out index, specificPosition))
                    return id;
                else break;

        }
        return kinectManager.GetPrimaryUserID();
    }

    public static bool GetClosestPlayer(ref KinectManager kinectManager, out long id, out int index, Vector3 positionWanted)
    {
        float lowestDistance = float.MaxValue;
        long idClosest = -1;
        int indexClosest = -1;
        id = -1; index = -1;
        for (int i = 0; i < kinectManager.GetUsersCount(); i++)
        {
            long userId = kinectManager.GetUserIdByIndex(i);
            Vector3 kinPos = kinectManager.GetUserPosition(userId);
            float dist = Vector3.Distance(kinPos, positionWanted);
            if (dist < lowestDistance)
            {
                lowestDistance = dist;
                idClosest = userId;
                indexClosest = i;
            }
            // Debug.Log(string.Format("Player vs Spot : {0}   -{1}-   {2}", kinPos,dist, positionWanted));
        }
        index = indexClosest;
        id = idClosest;
        return idClosest > 0;
    }
    public static float GetDistance(ref KinectManager kinectManager, Vector3 fromThisPosition)
    {
        float lowestDistance = float.MaxValue;
        long idClosest = -1;
        int indexClosest = -1;
        for (int i = 0; i < kinectManager.GetUsersCount(); i++)
        {
            long userId = kinectManager.GetUserIdByIndex(i);
            Vector3 kinPos = kinectManager.GetUserPosition(userId);
            float dist = Vector3.Distance(kinPos, fromThisPosition);
            if (dist < lowestDistance)
            {
                lowestDistance = dist;
                idClosest = userId;
                indexClosest = i;
            }
         //   Debug.Log(string.Format("Player vs Spot : {0}   -{1}-   {2}", kinPos, dist, fromThisPosition));
        }
        return lowestDistance;
    }




    public Vector3 GetDirectionCompareToSpecificPoint(bool withoutHeight=false)
    {
        KinectManager manager = KinectManager.Instance;
        long userId;
        int index;
        GetClosestPlayer(ref manager, out userId, out index, specificPosition);
        Vector3 kinPos = manager.GetUserPosition(userId);
        Vector3 dir = kinPos - specificPosition;
        dir.z *= -1f;
        if(withoutHeight)
        dir.y = 0f;
        return dir;
        
    }
}
