﻿using UnityEngine;
using System.Collections;

public class KinectGrappingHand_Holding : KinectGrappingHand 
{
    public KinectHandsBasicInfo kinectHandInfo;
    public enum GrabbingType { Default, HandClose, HandHolding }//HandMoveFront}
    public GrabbingType grabbingType;

    protected void Start() {
        base.Start();

        if (grabbingType == GrabbingType.HandClose)
        {
            SetStopUsingIf_Condition(StopHandCloseCondition);
            SetStartUsingIf_Condition(StartHandCloseCondition);
        }
        else if (grabbingType == GrabbingType.HandHolding)
        {
            SetStopUsingIf_Condition(StartHandHoldingCondition);
            SetStartUsingIf_Condition(StopHandHoldingCondition);
        }
    }

    private bool StartHandCloseCondition(HandInfo hand)
    {
        return hand.isClosed && hand.hasHandUp;
    }

    private bool StopHandCloseCondition(HandInfo hand)
    {

        return (!hand.isClosed && Mathf.Abs(hand.delta.magnitude) < 0.1f) || !hand.hasHandUp;
    }

    private bool StartHandHoldingCondition(HandInfo hand)
    {
        return hand.hasHandUp && hand.handNotMovingSince > 1.5f && hand.switchCoolDownCount <= 0;
    }

    private bool StopHandHoldingCondition(HandInfo hand)
    {
        return !hand.hasHandUp || (hand.handNotMovingSince > 2f && hand.switchCoolDownCount <= 0);
    }


    public override void GetHandsPosition(out Vector3 handLeftPos, out Vector3 handRightPos)
    {
        handLeftPos = kinectHandInfo.GetHandShoulder_Position(KinectHandsBasicInfo.SideType.Left, false);
        handRightPos = kinectHandInfo.GetHandShoulder_Position(KinectHandsBasicInfo.SideType.Right, false);
    }

    public override void GetHandsClosedState(out bool handLeftClosed, out bool handRightClosed)
    {
        handRightClosed = kinectHandInfo.Kin_RightHandClose;
        handLeftClosed = kinectHandInfo.Kin_LeftHandClose;
    }
}
