﻿using UnityEngine;
using System.Collections;


public class KinectHandsBasicInfo : MonoBehaviour
{
    public KinectUserSelection userSelected;
    public enum SideType { Left, Right, Unknown }

    #region Process variable
    public Vector3 KinPos_shoulderLeft;
    public Vector3 KinPos_shoulderRight;
    public Vector3 KinPos_handLeft;
    public Vector3 KinPos_handRight;

    public Vector3 KinPos_Neck;
    public Vector3 KinDir_Neck;
    public Quaternion KinQuat_Neck;

    public Vector3 OffsetKinPos_Neck;
    public Quaternion OffsetKinQuat_Neck;

    public Vector3 KinPos_Hands;
    public Vector3 KinDir_Hands;
    public Quaternion KinQuat_Hands;



    #region Hands close state
    private bool _leftHandClose;
    private bool _rightHandClose;
    public bool Kin_LeftHandClose
    {
        get { return _leftHandClose; }
        set
        {
            if (_leftHandClose == value)
                return;
            _leftHandClose = value;
            if (onHandChangeState != null)
                onHandChangeState(value, SideType.Left);
        }
    }
    public bool Kin_RightHandClose
    {
        get { return _rightHandClose; }
        set
        {
            if (_rightHandClose == value)
                return;
            _rightHandClose = value;
            if (onHandChangeState != null)
                onHandChangeState(value, SideType.Right);
        }
    }

    public delegate void OnHandChangeState(bool isClose, SideType side);
    public OnHandChangeState onHandChangeState;
    #endregion

    #endregion


    protected void Update()
    {

        UpdateKinectInfo();
    }

    #region Kinect position and offset
    private void UpdateKinectInfo()
    {

        KinectManager kinectManager = KinectManager.Instance;
        if (kinectManager && kinectManager.IsInitialized())
        {
            long userId = userSelected.GetUserId(ref kinectManager);

            if (userId != 0)
            {
                Kin_LeftHandClose = kinectManager.GetLeftHandState(userId) == KinectInterop.HandState.Closed;
                Kin_RightHandClose = kinectManager.GetRightHandState(userId) == KinectInterop.HandState.Closed;

                KinPos_handLeft = InverseZAxe(  kinectManager.GetJointPosition(userId, (int)KinectInterop.JointType.HandLeft));
                KinPos_handRight = InverseZAxe(  kinectManager.GetJointPosition(userId, (int)KinectInterop.JointType.HandRight));
                KinPos_shoulderLeft = InverseZAxe(  kinectManager.GetJointPosition(userId, (int)KinectInterop.JointType.ShoulderLeft));
                KinPos_shoulderRight = InverseZAxe(  kinectManager.GetJointPosition(userId, (int)KinectInterop.JointType.ShoulderRight));

                Define_2DFromTop_Direction(KinPos_handLeft, KinPos_handRight, ref KinDir_Hands, ref KinQuat_Hands, ref KinPos_Hands);
                Define_2DFromTop_Direction(KinPos_shoulderLeft, KinPos_shoulderRight, ref KinDir_Neck, ref KinQuat_Neck, ref KinPos_Neck);
            }
        }


        UpdateKinectInfoSafe();
    }

   

    private Vector3 InverseZAxe(Vector3 vector3)
    {
        vector3.z = - vector3.z;
        return vector3;
    }

    private static void Define_2DFromTop_Direction(Vector3 left, Vector3 right, ref Vector3 direction, ref Quaternion orientation, ref Vector3 centerPosition)
    {
        centerPosition = (left + right) / 2f;

        left.y = 0;
        right.y = 0;

        direction = Vector3.Cross(Vector3.up, (left - right));
        orientation = Quaternion.FromToRotation(Vector3.forward, direction);
    }

    public void ResetOffset()
    {
        OffsetKinPos_Neck = Vector3.zero;
        OffsetKinQuat_Neck = Quaternion.Euler(0, 0, 0);
    }

    public void SetOffset()
    {

        OffsetKinPos_Neck = KinPos_Neck;
        OffsetKinQuat_Neck = KinQuat_Neck;
    }

    #endregion


    public Vector3 GetHandShoulder_Position(SideType sideType, bool local = true, bool useSafeValue=true)
    {
        Vector3 hl = useSafeValue ? KinPos_handLeft_LastSafe : KinPos_handLeft;
        Vector3 hr = useSafeValue ? KinPos_handRight_LastSafe : KinPos_handRight;
        Vector3 sl = useSafeValue ? KinPos_shoulderLeft_LastSafe : KinPos_shoulderLeft;
        Vector3 sr = useSafeValue ? KinPos_shoulderRight_LastSafe : KinPos_shoulderRight;
        Quaternion neck = useSafeValue ? KinQuat_Neck_LastSafe : KinQuat_Neck;
        

        Vector3 d, o;
        d = SideType.Left == sideType ? hl: hr;
        o = SideType.Left == sideType ? sl : sr;


        if (local)
        {
            Vector3 pos = Quaternion.Inverse(neck) * (d - o);
            return pos;
        }
        else return d - o;
        
    }

    public float GetHandsDistance(bool useSafeValue=true)
    {
        Vector3 hl = useSafeValue ? KinPos_handLeft_LastSafe: KinPos_handLeft;
        Vector3 hr = useSafeValue ? KinPos_handRight_LastSafe : KinPos_handRight;
        return Vector3.Distance(hl, hr);
    }





    #region Last valide position variable and methodes
    private Vector3 _kinPos_shoulderLeft_LastSafe;
    private Vector3 _kinPos_shoulderRight_LastSafe;
    private Vector3 _kinPos_handLeft_LastSafe;
    private Vector3 _kinPos_handRight_LastSafe;

    private Vector3 _kinPos_Neck_LastSafe;
    private Vector3 _kinDir_Neck_LastSafe;
    private Quaternion _kinQuat_Neck_LastSafe;

    private Vector3 _offsetKinPos_Neck_LastSafe;
    private Quaternion _offsetKinQuat_Neck_LastSafe;
    private Vector3 _kinPos_Hands_LastSafe;
    private Vector3 _kinDir_HandsSafe_LastSafe;
    private Quaternion _kinQuat_HandsSafe_LastSafe;

    public Vector3 KinPos_shoulderLeft_LastSafe { get { return _kinPos_shoulderLeft_LastSafe; } set { if (value != Vector3.zero) _kinPos_shoulderLeft_LastSafe = value; } }
    public Vector3 KinPos_shoulderRight_LastSafe { get { return _kinPos_shoulderRight_LastSafe; } set { if (value != Vector3.zero) _kinPos_shoulderRight_LastSafe = value; } }
    public Vector3 KinPos_handLeft_LastSafe { get { return _kinPos_handLeft_LastSafe; } set { if (value != Vector3.zero) _kinPos_handLeft_LastSafe = value; } }
    public Vector3 KinPos_handRight_LastSafe { get { return _kinPos_handRight_LastSafe; } set { if (value != Vector3.zero) _kinPos_handRight_LastSafe = value; } }

    public Vector3 KinPos_Neck_LastSafe { get { return _kinPos_Neck_LastSafe; } set { if (value != Vector3.zero) _kinPos_Neck_LastSafe = value; } }
    public Vector3 KinDir_Neck_LastSafe { get { return _kinDir_Neck_LastSafe; } set { if (value != Vector3.zero) _kinDir_Neck_LastSafe = value; } }
    public Quaternion KinQuat_Neck_LastSafe { get { return _kinQuat_Neck_LastSafe; } set { if (value != Quaternion.identity) _kinQuat_Neck_LastSafe = value; } }

    public Vector3 OffsetKinPos_Neck_LastSafe { get { return _offsetKinPos_Neck_LastSafe; } set { if (value != Vector3.zero) _offsetKinPos_Neck_LastSafe = value; } }
    public Quaternion OffsetKinQuat_Neck_LastSafe { get { return _offsetKinQuat_Neck_LastSafe; } set { if (value != Quaternion.identity) _offsetKinQuat_Neck_LastSafe = value; } }

    public Vector3 KinPos_Hands_LastSafe { get { return _kinPos_Hands_LastSafe; } set { if (value != Vector3.zero) _kinPos_Hands_LastSafe = value; } }
    public Vector3 KinDir_HandsSafe_LastSafe { get { return _kinDir_HandsSafe_LastSafe; } set { if (value != Vector3.zero) _kinDir_HandsSafe_LastSafe = value; } }
    public Quaternion KinQuat_HandsSafe_LastSafe { get { return _kinQuat_HandsSafe_LastSafe; } set { if (value != Quaternion.identity) _kinQuat_HandsSafe_LastSafe = value; } }

    private void UpdateKinectInfoSafe()
    {
        KinPos_shoulderLeft_LastSafe = KinPos_shoulderLeft;
        KinPos_shoulderRight_LastSafe = KinPos_shoulderRight;
        KinPos_handLeft_LastSafe = KinPos_handLeft;
        KinPos_handRight_LastSafe = KinPos_handRight;

        KinPos_Neck_LastSafe = KinPos_Neck;
        KinDir_Neck_LastSafe = KinDir_Neck;
        KinQuat_Neck_LastSafe = KinQuat_Neck;

        OffsetKinPos_Neck_LastSafe = OffsetKinPos_Neck;
        OffsetKinQuat_Neck_LastSafe = OffsetKinQuat_Neck;

        KinPos_Hands_LastSafe = KinPos_Hands;
        KinDir_HandsSafe_LastSafe = KinDir_Hands;
        KinQuat_HandsSafe_LastSafe = KinQuat_Hands;

    }
    #endregion
}
