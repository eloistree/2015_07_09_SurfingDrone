﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class ConfigGestureListener : MonoBehaviour, KinectGestures.GestureListenerInterface {


    public List<KinectGestures.Gestures> gestureToListen;
    public List<long> user =new List<long>();

    public delegate void OnMovementDetected(long userId, int userIndex, KinectGestures.Gestures gesture, KinectInterop.JointType joint);
    public OnMovementDetected onMovementDetected;
    public delegate void OnUserDetected(long userId, int userIndex);
    public OnUserDetected onUserDetected;
    public delegate void OnUserLost(long userId, int userIndex);
    public OnUserLost onUserLost;

    public bool debug;

    public void UserDetected(long userId, int userIndex)
    {

        KinectManager manager = KinectManager.Instance;
        foreach (KinectGestures.Gestures gest in gestureToListen)
            manager.DetectGesture(userId, gest);

       if( ! user.Contains(userId))
           user.Add(userId);
       
        if(debug)Debug.Log("Track: " + userId);

       if (onUserDetected != null)
           onUserDetected(userId, userIndex);
    }

    public void UserLost(long userId, int userIndex)
    {
        if (user.Contains(userId))
            user.Remove(userId);

        if (debug) Debug.Log("Untrack: " + userId);

        if (onUserLost != null)
            onUserLost(userId, userIndex);
    }

    public void GestureInProgress(long userId, int userIndex, KinectGestures.Gestures gesture, float progress, KinectInterop.JointType joint, Vector3 screenPos)
    {
    }


    public bool GestureCompleted(long userId, int userIndex, KinectGestures.Gestures gesture, KinectInterop.JointType joint, Vector3 screenPos)
    {
        if ( ! BelongToListenedGesture(gesture)) return true;
        if (debug) Debug.Log("Gesture complete: " + gesture + "  (" + userId + ") -- " + joint + " -- " + screenPos);

        if (onMovementDetected != null)
            onMovementDetected(userId, userIndex, gesture, joint);
        return true;
    }

    private bool BelongToListenedGesture(KinectGestures.Gestures gesture)
    {
        return gestureToListen.Contains(gesture);
    }

    public bool GestureCancelled(long userId, int userIndex, KinectGestures.Gestures gesture, KinectInterop.JointType joint)
    {
        return true;
    }





    
}
