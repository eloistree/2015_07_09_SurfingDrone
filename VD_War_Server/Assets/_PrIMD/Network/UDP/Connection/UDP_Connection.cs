﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System.Net;

public class UDP_Connection : MonoBehaviour {

    public string connectionName="Client";
    public string seekingName="Server";

    public UDP_Sender sender;
    public UDP_Receiver receiver;

    public string linkedIp;


    public Coroutine seekForServer;

    public Queue<UDP_Message> messageReceived = new Queue<UDP_Message>();

	void Start () {
        StartListenToReceiver();
        StartSeekingServer();
	}

    void Update() {

        while(messageReceived.Count >= 1)
            MessageToCommand(messageReceived.Dequeue());
    }
    private void StartListenToReceiver()
    {
        receiver.onPackageReceived += ReceivedUdpMessage;
      
    }

    public void OnDestroy() {
        receiver.onPackageReceived -= ReceivedUdpMessage;
       
    }

    private void ReceivedUdpMessage(UDP_Receiver from, string _message, string _ip, int _port)
    {
        UDP_Message msg = new UDP_Message() { message = _message, ip = _ip, port = _port };

        messageReceived.Enqueue(msg);
    }
    private void MessageToCommand(UDP_Message udpMessage)
    {

       // Debug.Log("What("+messageReceived.Count+" ): "+udpMessage.message);
        string[] tokens = udpMessage.message.Split(':');

        if (tokens.Length == 2 && tokens[0].Equals("seekingmatch"))
        {
            if (tokens[1].CompareTo(seekingName)==0) { 
            
            linkedIp = udpMessage.ip;
            return;
            }
        }
            
        if (tokens.Length == 4 && tokens[0].Equals("Connection")) 
        {
            if (tokens[3].Equals(connectionName)) { 
                sender.Lock(udpMessage.ip, sender.port,true);
                sender.Send("seekingmatch:"+connectionName);
                sender.Unlock();
            }
        }
            

        





    }

    public bool IsConnected() {
        return !string.IsNullOrEmpty(linkedIp);
    }

        public IEnumerator SeekForServerCoroutine()
        {

            while(!IsConnected()){
            
            string [] ips = GetIpList();
            for (int i = 0; i < ips.Length; i++) {

                     if (IsConnected()) break;
                    string ipToCheck = ips[i];
                    string[] ipToken = ipToCheck.Split('.');
                    string ipZone = ipToken[0] + "." + ipToken[1] + "." + ipToken[2] + ".";
                    for (int ipCount = 0; ipCount <= 255; ipCount++) {

                        if (IsConnected()) break;
                        sender.TargetClientToSend(ipZone + ipCount, sender.port, true);
                        sender.Send(string.Format("Connection:{0}:to:{1}", connectionName, seekingName));
                        if (ipCount % 5 == 0) yield return new WaitForSeconds(0.03f);
                    }

                }
            
            }
            sender.TargetClientToSend(linkedIp, sender.port, true);
        }

        

        public void StartSeekingServer() {

            if (seekForServer != null)
                StopCoroutine(seekForServer);
            linkedIp = "";
            seekForServer = StartCoroutine(SeekForServerCoroutine());
        }

        public void StopSeekingServer() {
            StopCoroutine(seekForServer);
        
        }

        public static string [] GetIpList() {
            string strHostName = Dns.GetHostName(); 
            IPHostEntry ipEntry = Dns.GetHostEntry(strHostName);

            int ipCount = ipEntry.AddressList.Length;
            string [] ipAdresses = new string[ipCount];
            for(int i=0; i<ipCount;i++)
                ipAdresses[i] = ipEntry.AddressList[i].ToString();
            return ipAdresses;
        }

        public struct UDP_Message {
            public string message;
            public string ip;
            public int port;
        }
}


