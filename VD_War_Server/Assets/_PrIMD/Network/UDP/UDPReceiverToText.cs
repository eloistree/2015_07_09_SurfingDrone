﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UDPReceiverToText : MonoBehaviour
{

    public UDP_Receiver receiver;
    public Text tDisplayValue;
    public string toDisplay;

    public void Start()
    {
        receiver.onPackageReceived += RefreshReceived;

    }
    public void RefreshReceived(UDP_Receiver from, string message, string adresse, int port)
    {
        toDisplay =  message;

    }
    public void Update()
    {
        tDisplayValue.text = "In: " + toDisplay;

    }
}