﻿using UnityEngine;
using System.Collections;

public class APU_Control : MonoBehaviour {

    public string kinectControllerName="KinectInfo";
    public VirtualController kinectController;
    public string gearControllerName = "GearInfo";
    public VirtualController gearController;


    public Transform root;
    public Transform gunLeft;
    public Transform gunRight;
    public float gunDistance = 1f;

    public float moveSpeed = 1f;

    void Start()
    {
        kinectController = VirtualController.Get(kinectControllerName);
        gearController = VirtualController.Get(gearControllerName);

    }
	void Update () {
        root.rotation = gearController.GetOrientation(0).Value;
        gunLeft.localPosition = kinectController.GetPosition(0).Value * gunDistance;
        gunRight.localPosition = kinectController.GetPosition(1).Value * gunDistance;

        gunLeft.localRotation = Quaternion.LookRotation(gunLeft.localPosition);
        gunRight.localRotation = Quaternion.LookRotation(gunRight.localPosition);

        Vector3 moveDirection = kinectController.GetPosition(2).Value;
        root.Translate(moveDirection * moveSpeed * Time.deltaTime);

        
	
	}
}
