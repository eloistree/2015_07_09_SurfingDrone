﻿using UnityEngine;
using System.Collections;

public class GoliathInfo : MonoBehaviour {

    public KinectUserSelection userSelection;
    public bool IsFiringLeft;// { get; private set; }
    public bool IsFiringRight;// { get; private set; }
    public bool IsShielded;//{ get; private set; }
    public bool IsScaning;//{ get; private set; }
    public Vector3 Direction;// { get; private set; }
    public Vector3 DirectionLeftArm;// { get; private set; }
    public Vector3 DirectionRightArm;// { get; private set; }
    public Vector3 DirectionSpine;
    public KinectHandsBasicInfo handsInfo;

    public Tracked_Group cond_firingLeft;
    public Tracked_Group cond_firingRight;
    public Tracked_Group cond_shieldActive;
    public Tracked_Group cond_scanActive;

    public Transform neck;
    public Transform spine;

	void Update () {


        if (cond_firingLeft)
            IsFiringLeft = cond_firingLeft.GetGroupCondition().IsComplete();
        if (cond_firingRight)
            IsFiringRight = cond_firingRight.GetGroupCondition().IsComplete();
        if (cond_shieldActive)
            IsShielded = cond_shieldActive.GetGroupCondition().IsComplete();
        if (cond_scanActive)
            IsScaning = cond_scanActive.GetGroupCondition().IsComplete();

        Direction = userSelection.GetDirectionCompareToSpecificPoint();
        DirectionLeftArm = handsInfo.GetHandShoulder_Position(KinectHandsBasicInfo.SideType.Left, false, true);
        DirectionRightArm = handsInfo.GetHandShoulder_Position(KinectHandsBasicInfo.SideType.Right, false, true);
        DirectionSpine = neck.position - spine.position;


	}
}
