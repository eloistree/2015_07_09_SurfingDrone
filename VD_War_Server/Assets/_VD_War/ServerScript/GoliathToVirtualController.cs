﻿using UnityEngine;
using System.Collections;

public class GoliathToVirtualController : MonoBehaviour {

    public string controllerName = "GoliathInput";
    public GoliathInfo goliathInfo;
    public VirtualController controller;
    public void Start() {
        controller = VirtualController.Get(controllerName);   
    }

    public void Update() {

        controller.SetButtonValue(0, goliathInfo.IsFiringLeft);
        controller.SetButtonValue(1, goliathInfo.IsFiringRight);
        controller.SetButtonValue(2, goliathInfo.IsShielded);
        controller.SetButtonValue(3, goliathInfo.IsScaning);

        controller.SetPositionValue(0, goliathInfo.DirectionLeftArm);
        controller.SetPositionValue(1, goliathInfo.DirectionRightArm);
        controller.SetPositionValue(2, goliathInfo.Direction);
        controller.SetPositionValue(3, goliathInfo.DirectionSpine);
 
    
    }


}
