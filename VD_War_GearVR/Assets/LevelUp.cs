﻿using UnityEngine;
using System.Collections;

public class LevelUp : MonoBehaviour {

    public MeteorSender meteorSender;
    public float levelDelay = 5f;
    public int levelCount = 0;

	void Start () {
        InvokeRepeating("LevelUpImpl", 0, levelDelay);
	}

    void LevelUpImpl()
    {

        meteorSender.minSpeed *= 1.02f;
        meteorSender.maxSpeed *= 1.02f;

        meteorSender.minTimeBetween *= 0.97f;
        meteorSender.maxTimeBetween *= 0.98f;
        levelCount++;
    }
	
}
