﻿using UnityEngine;
using System.Collections;

public class KillPlayerIfToFar : MonoBehaviour {

    public Transform playerPosition;
    public float maxDistance=60f;
	void Update () {
        Vector3 pos = playerPosition.position;
        pos.z = 0f;
        if (pos.magnitude > maxDistance)
        {
            playerPosition.BroadcastMessage("Explode", SendMessageOptions.DontRequireReceiver);
            playerPosition.gameObject.SetActive(false);
        }
	
	}
}
