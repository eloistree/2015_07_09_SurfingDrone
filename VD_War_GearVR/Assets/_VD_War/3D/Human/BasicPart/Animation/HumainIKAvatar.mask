%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!1011 &101100000
AvatarMask:
  m_ObjectHideFlags: 0
  m_PrefabParentObject: {fileID: 0}
  m_PrefabInternal: {fileID: 0}
  m_Name: HumainIKAvatar
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: ArmLeft
    m_Weight: 1
  - m_Path: ArmRight
    m_Weight: 1
  - m_Path: Body
    m_Weight: 1
  - m_Path: FootLeft
    m_Weight: 1
  - m_Path: FootRight
    m_Weight: 1
  - m_Path: ForearmLeft
    m_Weight: 1
  - m_Path: ForearmRight
    m_Weight: 1
  - m_Path: HandLeft
    m_Weight: 1
  - m_Path: HandRight
    m_Weight: 1
  - m_Path: Head
    m_Weight: 1
  - m_Path: metarig
    m_Weight: 1
  - m_Path: metarig/hips
    m_Weight: 1
  - m_Path: metarig/hips/spine
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/neck
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/neck/head
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/upper_arm_L
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/upper_arm_L/forearm_L
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/upper_arm_L/forearm_L/forearm_L_001
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/upper_arm_R
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/upper_arm_R/forearm_R
    m_Weight: 1
  - m_Path: metarig/hips/spine/chest/upper_arm_R/forearm_R/forearm_R_001
    m_Weight: 1
  - m_Path: metarig/hips/thigh_L
    m_Weight: 1
  - m_Path: metarig/hips/thigh_L/shin_L
    m_Weight: 1
  - m_Path: metarig/hips/thigh_L/shin_L/foot_L
    m_Weight: 1
  - m_Path: metarig/hips/thigh_R
    m_Weight: 1
  - m_Path: metarig/hips/thigh_R/shin_R
    m_Weight: 1
  - m_Path: metarig/hips/thigh_R/shin_R/foot_R
    m_Weight: 1
  - m_Path: ShinLeft
    m_Weight: 1
  - m_Path: ShinRight
    m_Weight: 1
  - m_Path: ThighRight
    m_Weight: 1
  - m_Path: TightLeft
    m_Weight: 1
