﻿using UnityEngine;
using System.Collections;

public class HandyDrone : MonoBehaviour {

    public Transform body;
    public float speed = 0;
    public float maxSpeed = 10f;
    public float acceleration = 30f;
    private bool accelerating;

    public float rotationSpeed = 90f;

    public Vector3 lastMovingDirection = Vector3.forward;
    public Vector3 movingDirection = Vector3.zero;
	void Start () {
	

	}

    public void SetAccelerate(bool onOff) {
        accelerating = onOff;
    }
    public void SetDirection(Vector3 direction) {
 
        movingDirection = direction;
        lastMovingDirection = direction;
    }
	
	void Update () {

        if (accelerating && speed < maxSpeed)
        {
            speed += (0.5f+speed)*acceleration * Time.deltaTime;
            if (speed > maxSpeed) speed = maxSpeed;
        }else if (!accelerating && speed > 0f)
        {
            speed -= acceleration*2f * Time.deltaTime;
            if (speed <0f) speed = 0f;
        }

        if (speed > 0f) {

            Vector3 newDirection = movingDirection;
            if(newDirection!=Vector3.zero)
                lastMovingDirection = newDirection;
            body.position = Vector3.MoveTowards(body.position, body.position+newDirection,(speed * Time.deltaTime) );
//            body.position += newDirection * ;
        }


        Quaternion wantedRotation = Quaternion.identity;

        Vector3 tilt = Vector3.zero ;
        if(accelerating)  {
        tilt=lastMovingDirection;
        tilt.y=0;
        }

        Quaternion tiltFoward = Quaternion.Euler(new Vector3( tilt.z * 35f * (speed/maxSpeed),0, 0));
        Quaternion tiltLateral = Quaternion.Euler(new Vector3(0, 0, tilt.x * -35f) * (speed / maxSpeed));
        wantedRotation *= tiltLateral;
        wantedRotation *= tiltFoward;
       // wantedRotation = inclination * wantedRotation;

        body.rotation = Quaternion.RotateTowards(body.rotation, wantedRotation, rotationSpeed * Time.deltaTime);
	
	}

    internal Vector3 GetPosition()
    {
        return body.position;
    }

    internal float GetSpeed()
    {
        return speed;
    }

    internal void SetSpeed(float value)
    {
        speed = value;
    }
}
