﻿using UnityEngine;
using System.Collections;

public class DroneV1 : MonoBehaviour {

    public float speed = 5f;
    public GameObject playerPosition;
    public float playerDistanceWanted=10f;
	void Start () {
        PopSetup();
	}
	
	void Update () {

        Vector3 direction = playerPosition.transform.position-transform.position;
        if (direction.magnitude > playerDistanceWanted) {
            transform.position += direction.normalized * (speed * Time.deltaTime);
        }
        transform.rotation = Quaternion.LookRotation(direction.normalized);
	
	}

    public void OnCollisionEnter(Collision col) {

        Destroy(this.gameObject);
    
    }

    public void PopSetup() {
        playerPosition = GameObject.FindWithTag("Player");
       // transform.position = new Vector3(Random.Range(-50, 50), 5, Random.Range(-50, 50));
	
    }
}
