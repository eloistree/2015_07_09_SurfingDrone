﻿using UnityEngine;
using System.Collections;

public class MeteorSender : MonoBehaviour {

    public bool senderActive = true;
    public MeteorCreator meteorCreator;
    public float minTimeBetween = 0.3f, maxTimeBetween = 3f;


    public Transform from, to;
    public float rangeRadius=10f;

    public float minSpeed = 5f, maxSpeed = 20f;
    public float minSize = 0.5f, maxSize = 5f;

    public Coroutine coroutine_CreateMeteor;

	void OnEnable () {

        coroutine_CreateMeteor = StartCoroutine(SendRandomMeteor());
	}

    IEnumerator SendRandomMeteor() {


        while (true) {
            if (!senderActive)
                yield return new WaitForSeconds(1f);

            Vector3 where = from.position + new Vector3(Random.Range(-rangeRadius, rangeRadius), Random.Range(-rangeRadius, rangeRadius), Random.Range(-rangeRadius, rangeRadius));
            Vector3 direction = (to.position - from.position).normalized;
            float speed = Random.Range(minSpeed, maxSpeed);
            float size = Random.Range(minSize, maxSize);
            meteorCreator.CreateMeteor(where, direction, speed,size);
            float nextTime = Random.Range(minTimeBetween, maxTimeBetween);
            yield return new WaitForSeconds(nextTime);

        
        }
    
    }
}
