﻿using UnityEngine;
using System.Collections;

public class APU_Control : MonoBehaviour {

    public string kinectControllerName="ComputerInfo";
    public VirtualController kinectController;
    public string gearControllerName = "MobileInfo";
    public VirtualController gearController;


    public Transform root;
    public Transform gunLeftPosition;
    public Transform gunRightPosition;
    public float gunDistance = 1f;

    public float moveSpeed = 1f;

    public CrystalGun gunLeft;
    public CrystalGun gunRigh;
    public RocketGun rocketLeft;
    public RocketGun rocketRigh;
    public Transform shield;
    public Transform scan;

    void Start()
    {
        kinectController = VirtualController.Get(kinectControllerName);
        gearController = VirtualController.Get(gearControllerName);

    }
	void Update () {
        // root.rotation = gearController.GetOrientation(0).Value;
        gunLeftPosition.localPosition = kinectController.GetPosition(0).Value * gunDistance;
        gunRightPosition.localPosition = kinectController.GetPosition(1).Value * gunDistance;

        gunLeftPosition.localRotation = Quaternion.LookRotation(gunLeftPosition.localPosition);
        gunRightPosition.localRotation = Quaternion.LookRotation(gunRightPosition.localPosition);

        Vector3 moveDirection = kinectController.GetPosition(2).Value;
        if(moveDirection.magnitude>0.2f)
        root.Translate(moveDirection * moveSpeed * Time.deltaTime);

        bool isFiringLeft = kinectController.GetButton(0).Value;
        bool isFiringRight = kinectController.GetButton(1).Value;
        
        if (isFiringLeft & !FiringLeftState)
        {
            if (GetTime() - lastTimeOutFiringLeft < 1f)
                rocketLeft.Fire();
            else gunLeft.SetFire(true);

        }
        else {
            if(!isFiringLeft)
            gunLeft.SetFire(false);
        
        }
        if (isFiringRight & !FiringRightState)
        {
            if (GetTime() - lastTimeOutFiringRight < 0.5f)
                rocketRigh.Fire();
            else gunRigh.SetFire(true);

        }
        else
        {
            if (!isFiringLeft)
                gunRigh.SetFire(false);

        }


        
        
            shield.gameObject.SetActive(kinectController.GetButton(2).Value);
            scan.gameObject.SetActive(kinectController.GetButton(3).Value);
            FiringLeftState = isFiringLeft;
            FiringRightState = isFiringRight;
        	
	}

    private bool _firingLeft;

    public bool FiringLeftState
    {
        get { return _firingLeft; }
        set
        {
            if (_firingLeft && !value)
                lastTimeOutFiringLeft = GetTime();
            _firingLeft = value;
        }
    }
    private bool _firingRight;

    public bool FiringRightState
    {
        get { return _firingRight; }
        set
        {
            if (_firingRight && !value)
                lastTimeOutFiringRight = GetTime();
            _firingRight = value;
        }
    }

    private float GetTime()
    {
        return Time.time;
    }

    public float lastTimeOutFiringLeft;
    public float lastTimeOutFiringRight;
}


