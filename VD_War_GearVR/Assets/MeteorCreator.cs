﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MeteorCreator : MonoBehaviour {

    public string [] poolsName;
    public List<PoolGenerator> pools= new List<PoolGenerator>();
	void Start () {
        for (int i = 0; i < poolsName.Length; i++)
        {
            PoolGenerator pool = PoolGenerator.GetPool(poolsName[i]); 
            if(pool)
                pools.Add(pool);
        }
	}


    public void CreateMeteor(Vector3 where, Vector3 direction, float speed, float size)
    {

        GameObject meteor = GetMeteor();
        if (meteor == null) return;

        meteor.transform.position = where;
        if (size != 0f) 
        meteor.transform.localScale = Vector3.one * size;

        QuickMoveFoward moveFoward = meteor.GetComponent<QuickMoveFoward>();
        if (!moveFoward)
            moveFoward = meteor.AddComponent<QuickMoveFoward>() as QuickMoveFoward;
        if (moveFoward) {

            moveFoward.SetSpeed(  speed);
            moveFoward.SetFoward(direction);
        }
        meteor.SetActive(true);

    }

    public GameObject GetMeteor() {
        if (pools.Count <= 0) return null;
        int index = Random.Range(0, pools.Count);
        return pools[index].GetNextAvailable();
    }
}
