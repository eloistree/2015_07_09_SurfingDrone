﻿using UnityEngine;
using System.Collections;

public class NextLevelTimer : MonoBehaviour {

    public float countDown = 10f;
    public LoadScene loader;

	void Update () {
        countDown -= Time.deltaTime;
        if (countDown < 0f)
            loader.LoadSelectedSceneWithDelay();

	}
}
