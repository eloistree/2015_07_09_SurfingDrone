﻿using UnityEngine;
using System.Collections;

public class MoveIntheMiddle : MonoBehaviour {

    public Transform[] stuffs;
    public float speed=1f;
	
	void Update () {

        Vector3 middle = GetMiddle() ;
        middle.z = 0;
        transform.position = Vector3.Slerp(transform.position, middle, Time.deltaTime * speed);

	
	}

    public Vector3 GetMiddle() {

        if (stuffs.Length == 0) return Vector3.zero;

        Vector3 middle = stuffs[0].position;
        if (stuffs.Length == 1) return stuffs[0].position;

        for (int i = 1; i < stuffs.Length; i++)
        {
            middle += stuffs[i].position;

        }

        return middle / stuffs.Length;


    }
}
