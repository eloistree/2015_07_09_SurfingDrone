﻿using UnityEngine;
using System.Collections;

public class DisplaytInputSelected : MonoBehaviour {

    public InputSelector inputSelector;

    public Transform kinect;
    public Transform controller;
    public Transform gearVR;

	void Start () {
        inputSelector = GameObject.FindObjectOfType<InputSelector>() as InputSelector;
        InvokeRepeating("CheckSelection",0, 0.2f);
	}
	
	void CheckSelection () {

        if (!inputSelector) return;

        kinect.gameObject.SetActive(inputSelector.selection == InputSelector.ControlSelected.Kinect);
        controller.gameObject.SetActive(inputSelector.selection == InputSelector.ControlSelected.Gamepad);
        gearVR.gameObject.SetActive(inputSelector.selection == InputSelector.ControlSelected.View);

	
	}
}
