﻿using UnityEngine;
using System.Collections;

public class CointSound : MonoBehaviour {

    public AudioSource cointSound;
	void Start () {
        CointScore.onCointRecolted += PlayCointSound;
	}

    private void PlayCointSound()
    {
        cointSound.Play();
    }

    void OnDestroy() {

        CointScore.onCointRecolted -= PlayCointSound;
    }
	
}
