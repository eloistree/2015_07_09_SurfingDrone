﻿using UnityEngine;
using System.Collections;

public class CrystalGun : MonoBehaviour {

    public string poolName;
    public PoolGenerator ammoPool;
    public float bulletDelay = 0.3f;
    public Transform ammoStartPosition;
    public bool fire;

	void Start () {
        if(ammoPool==null)
        ammoPool = PoolGenerator.GetPool(poolName);
        InvokeRepeating("Fire", 0, bulletDelay);
	}

    void Fire() {
        if (IsFiring())
        {
            GameObject ammo = ammoPool.GetNextAvailable();
            ammo.transform.position = ammoStartPosition.position;
            ammo.transform.rotation = ammoStartPosition.rotation;
            ammo.SetActive(true);
        }
    }

    public void SetFire(bool onOff){
        fire = onOff;
    }

    public bool IsFiring() {
        return fire || Input.GetMouseButton(0);

    }
}
