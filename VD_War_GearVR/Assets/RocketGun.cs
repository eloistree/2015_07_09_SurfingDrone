﻿using UnityEngine;
using System.Collections;

public class RocketGun : MonoBehaviour {

    public string poolName;
    public PoolGenerator ammoPool;
    public Transform ammoStartPosition;

    void Start()
    {
        if (ammoPool == null)
            ammoPool = PoolGenerator.GetPool(poolName);
    }

    public void Fire()
    {
            GameObject ammo = ammoPool.GetNextAvailable();
            ammo.transform.position = ammoStartPosition.position;
            ammo.transform.rotation = ammoStartPosition.rotation;
            ammo.SetActive(true);
    }


}
