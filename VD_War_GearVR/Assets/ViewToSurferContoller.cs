﻿using UnityEngine;
using System.Collections;

public class ViewToSurferContoller : MonoBehaviour {

	    public string surferControllerName = "SurferInput";
    public VirtualController surferController;

    Vector3 playerDirection;
    Vector3 playerTilt;
    Vector3 directionDroneLeft;
    Vector3 directionDroneRight;

    Vector3 playerDirectionLerped;
    Vector3 playerTiltLerped;
    Vector3 directionDroneLeftLerped;
    Vector3 directionDroneRightLerped;


    void Start()
    {
        surferController = VirtualController.Get(surferControllerName);
    }
    void Update()
    {



        if (OculusCurrentOrientation.IsDefined())
        {
            Quaternion headOrientation = OculusCurrentOrientation.GetOrientation();
            Vector3 eulerHeadOri = headOrientation.eulerAngles;
            Debug.Log("Euleur: " + eulerHeadOri);

            Vector3 direction = OculusCurrentOrientation.Instance.center.forward;

            float headTilt = 0;
            if (eulerHeadOri.z < 180f)
                headTilt = -eulerHeadOri.z;
            else headTilt = -(eulerHeadOri.z - 360);

            //float headFrontalTilt = 0;
            //if (eulerHeadOri.x < 180f)
            //    headFrontalTilt = -eulerHeadOri.x;
            //else headFrontalTilt = -(eulerHeadOri.x - 360);


            //float headRotation = 0;
            //if (eulerHeadOri.y < 180f)
            //    headRotation = eulerHeadOri.y;
            //else headRotation = (eulerHeadOri.y - 360);


            //playerTilt.x = -headTilt / 60f;
            //playerTilt.z = direction.y;
            directionDroneLeft.z = directionDroneRight.z = 0.5f;

            if (direction.z < 0.7f)
            {
                directionDroneLeft.x = directionDroneRight.x = 0;
                directionDroneLeft.y = directionDroneRight.y = 0;
                    }
            else{
                directionDroneLeft.x = directionDroneRight.x = direction.x*3f;
                directionDroneLeft.y = direction.y*3f+headTilt/90f;
                directionDroneRight.y = direction.y * 3f - +headTilt / 90f;
    
            }

                playerDirection.x = -headTilt / 60f;
        }

        directionDroneLeftLerped = Vector3.Lerp(directionDroneLeftLerped, directionDroneLeft, Time.deltaTime * 2f);
        directionDroneRightLerped = Vector3.Lerp(directionDroneRightLerped, directionDroneRight, Time.deltaTime * 2f);

        playerTiltLerped = Vector3.Lerp(playerTiltLerped, playerTilt, Time.deltaTime);
        playerDirectionLerped = Vector3.Lerp(playerDirectionLerped, playerDirection, Time.deltaTime * 2f);


        surferController.SetJoystickValue(0, directionDroneLeftLerped);
        surferController.SetJoystickValue(1, directionDroneRightLerped);
        surferController.SetJoystickValue(2, playerDirectionLerped);
        surferController.SetJoystickValue(3, playerTiltLerped);
    }
}
