﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextSwitcher : MonoBehaviour {

    public int index;
    public string[] textesToSwitch;
    public float delay = 2f;
    public Text tStartGame;

	// Use this for initialization
	void Start (){
        InvokeRepeating("Switch", 0, delay);
	}
	
	// Update is called once per frame
	void Switch() {
        if(index<textesToSwitch.Length)
        tStartGame.text = textesToSwitch[index];
        index++;
	
	}
}
