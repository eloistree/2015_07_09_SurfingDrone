﻿using UnityEngine;
using System.Collections;

public class VritualControllerToSurferController : MonoBehaviour
{

    public string controllerName= "SurferInput";
    public VirtualController virtualControl;
    public SurferControl surferControl;
//    public SurferMover surferMove;

    void Start()
    {
        virtualControl = VirtualController.Get(controllerName);
        Init();

    }

    private void Init()
    {
        virtualControl.GetJoystick(0).onValueChanged += JoystickLeftControl;
        virtualControl.GetJoystick(1).onValueChanged += JoystickRightControl;
        virtualControl.GetJoystick(2).onValueChanged += PlayerDirectionControl;
        virtualControl.GetJoystick(3).onValueChanged += PlayerSpineDirectionControl;
    
    }
    void OnDestroy() {
        virtualControl.GetJoystick(0).onValueChanged -= JoystickLeftControl;
        virtualControl.GetJoystick(1).onValueChanged -= JoystickRightControl;
        virtualControl.GetJoystick(2).onValueChanged -= PlayerDirectionControl;
        virtualControl.GetJoystick(3).onValueChanged -= PlayerSpineDirectionControl;
    
    }

    private void PlayerSpineDirectionControl(int index, Vector3 oldValue, Vector3 newValue)
    {
        surferControl.SetPlayerSpineDirection(newValue);
    }
    private void PlayerDirectionControl(int index, Vector3 oldValue, Vector3 newValue)
    {
        surferControl.SetPlayerDirection(newValue);
    }

    private void JoystickRightControl(int index, Vector3 oldValue, Vector3 newValue)
    {
        surferControl.SetHandRightDirection(newValue);
    }

    private void JoystickLeftControl(int index, Vector3 oldValue, Vector3 newValue)
    {
        surferControl.SetHandLeftDirection(newValue);
    }
}
