﻿using UnityEngine;
using System.Collections;

public class ExplodeOnDestroy : MonoBehaviour {

    public string poolName = "Explosion";
    PoolGenerator pool;

    void Start() {
      CheckPool();
    }

    private void CheckPool()
    {
        if (pool == null)
            pool = PoolGenerator.GetPool(poolName);
    }

    void OnDisable() {

        if (pool == null)
        { CheckPool();
           if (pool == null) return;

        }

        GameObject explosion = pool.GetNextAvailable();
        explosion.transform.position = transform.position;
        explosion.SetActive(true);
    }
}
