﻿using UnityEngine;
using System.Collections;

public class DroneEditorInterface : MonoBehaviour {

    public HandyDrone handyDrone;
    public Transform followedPoint;
    public float acceleratingDistance = 2f;

    void Update () {
        if (!followedPoint) return;
        Vector3 dronePosition = handyDrone.GetPosition();
        float distanceOfObjectif = Vector3.Distance(dronePosition, followedPoint.position);

        handyDrone.SetAccelerate(distanceOfObjectif > acceleratingDistance);
        handyDrone.SetDirection((followedPoint.position - dronePosition).normalized);
        if (distanceOfObjectif < acceleratingDistance/2f && handyDrone.GetSpeed() > 0.5f)
            handyDrone.SetSpeed(0.5f);
 
	}

}
