﻿using UnityEngine;
using System.Collections;

public class PlayerDeathChecker : MonoBehaviour {

    public GameObject playerTarget;
    public LoadScene sceneToLoad;

	void Start () {

        InvokeRepeating("Check",0, 0.5f);
	}
	
	// Update is called once per frame
	void Check () {
        if (playerTarget == null || playerTarget.activeSelf == false)
        {
            sceneToLoad.LoadSelectedSceneWithDelay();
            Destroy(this);
        }
	}
}
