﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

    public HandyDrone drone;
    public Transform origine;
    public bool left;

    public void Update()
    {

        Vector3 dronePosition = drone.GetPosition();
        Vector3 dirFromOrigine = dronePosition - origine.position;

        Vector3 direction = Vector3.zero;
        if (IsAccelerating(left))
            direction.z += 1f;
        else if (dirFromOrigine.z > 0f)
            direction.z -= 1f;

        direction.x -= GetHorizontal(left);
        direction.y -= GetVertical(left);

        drone.SetAccelerate(direction == Vector3.zero);
        drone.SetDirection(direction); 

    }

    public bool IsAccelerating(bool left = true) {


        if (left && OVRGamepadController.GPC_GetButton(OVRGamepadController.Button.LeftShoulder))
            return true;
        else if (!left && OVRGamepadController.GPC_GetButton(OVRGamepadController.Button.RightShoulder))
            return true;
        return false;
    }

    public float GetHorizontal(bool left = true)
    {
        float value = 0f;
        if (left)
        {
            value = OVRGamepadController.GPC_GetAxis(OVRGamepadController.Axis.LeftXAxis);
            if (value != 0f) return -value;
            value = Input.GetAxis("Desktop_Left_Y_Axis");
            if (value != 0f) return -value;
        }
        else
        {

            value = OVRGamepadController.GPC_GetAxis(OVRGamepadController.Axis.RightXAxis);
            if (value != 0f) return -value;
            value = Input.GetAxis("Desktop_Right_X_Axis");
            if (value != 0f) return -value;
        }
        return value;
    }

    public float GetVertical(bool left = true)
    {
        float value = 0f;
        if (left)
        {
            value = OVRGamepadController.GPC_GetAxis(OVRGamepadController.Axis.LeftYAxis);
            if (value != 0f) return -value;
            value = Input.GetAxis("Desktop_Left_Y_Axis");
            if (value != 0f) return -value;
        }
        else
        {

            value = OVRGamepadController.GPC_GetAxis(OVRGamepadController.Axis.RightYAxis);
            if (value != 0f) return -value;
            value = Input.GetAxis("Desktop_Right_Y_Axis");
            if (value != 0f) return -value;
        }
        return value;
    }


    //bool moveForward = Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow);
    //bool moveLeft = Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow);
    //bool moveRight = Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow);
    //bool moveBack = Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow);

    //bool dpad_move = false;

    //if (OVRGamepadController.GPC_GetButton(OVRGamepadController.Button.Up))
    //{
    //    moveForward = true;
    //    dpad_move   = true;

    //}
    //if (OVRGamepadController.GPC_GetButton(OVRGamepadController.Button.Down))
    //{
    //    moveBack  = true;
    //    dpad_move = true;
    //}

}
