﻿using UnityEngine;
using System.Collections;

public class SurfWithDroneMove : MonoBehaviour {

    public Transform playerToMove;
    public SurferControl surfcontrol;
    public float minAngleToLateralMove=30;
    public float maxAngleToLateralMove=90;

    public float minHeightToVerticalMove=0.5f;
    public float maxHeightToVerticalMove = 1f;

    public float lateralSpeed = 10f;
    public float verticalSpeed = 10f;

    public float dronesAngle;
    public float dronesHeight;
	
	void Update () {

        Vector3 droneLeftPos = surfcontrol.GetDroneLeft().position ;
        Vector3 droneRightPos = surfcontrol.GetDroneRight().position;
        Vector3 playerDirection = surfcontrol.GetPlayerDirection(true);
        droneLeftPos.z = 0;
        droneRightPos.z = 0;
        Vector3 leftToRight = droneRightPos - droneLeftPos;
        dronesAngle = Vector3.Angle(Vector3.right, leftToRight) * -Mathf.Sign(leftToRight.y);

        // Move vertical with player incination;
        //Vector3 spineDirection = surfcontrol.GetSpineDirection(true).normalized;
        //spineDirection.x = 0;
        //angleVertical = Vector3.Angle(Vector3.up, spineDirection) * -Mathf.Sign(spineDirection.z);


        float dronesHeight = (surfcontrol.GetHandLeft().y + surfcontrol.GetHandRight().y) / 2f;


        Vector3 toMove = Vector3.zero;
        toMove.x += dronesAngle / maxAngleToLateralMove * lateralSpeed * Time.deltaTime;
        toMove.y += dronesHeight / maxHeightToVerticalMove * verticalSpeed * Time.deltaTime;

        if(Mathf.Abs(playerDirection.x)>0.8f)
            toMove.x += playerDirection.x * lateralSpeed * Time.deltaTime;

        playerToMove.position += toMove;
        
	}
}
