﻿using UnityEngine;
using System.Collections;

public class CointScore : MonoBehaviour {

    public static int score;
    public static string playerPrefScore = "PlayerPrefScore";

    public string levelName = "SurferGame";
    public delegate void OnCointRecolted();
    public static OnCointRecolted onCointRecolted;


  
     public void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.CompareTag("Collector") || col.gameObject.CompareTag("Player")) { 
            score++;
            if (onCointRecolted != null)
                onCointRecolted();
        }
        

    }
    public static void ResetScore(){

        CointScore.score = 0;
    }


    public static int GetBestScore() { return PlayerPrefs.GetInt(playerPrefScore, 0); }
    public static void SaveScore(int score)
    {

        int bestScore = GetBestScore();
        if (score > bestScore)
            PlayerPrefs.SetInt(playerPrefScore, score);
    }


    internal static void SaveScore()
    {
        SaveScore(CointScore.score);
    }

   
}
