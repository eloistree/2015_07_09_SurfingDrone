﻿using UnityEngine;
using System.Collections;

public class LinkedLine : MonoBehaviour {


    public LineRenderer line;
    public Transform origine;
    public Transform destination;

    void Start() {

        line.SetVertexCount(2);
    }

    void Update()
    {
        line.SetPosition(0, origine.position);
        line.SetPosition(1, destination.position);
    
    }
}
