﻿using UnityEngine;
using System.Collections;

public class GamePadToSurferController : MonoBehaviour {

    public string surferControllerName = "SurferInput";
    public VirtualController surferController;

    Vector3 playerDirection;
    Vector3 playerTilt;
    Vector3 directionDroneLeft;
    Vector3 directionDroneRight;

    Vector3 playerDirectionLerped;
    Vector3 playerTiltLerped;
    Vector3 directionDroneLeftLerped;
    Vector3 directionDroneRightLerped;


    void Start()
    {
        surferController = VirtualController.Get(surferControllerName);
    }
    void Update() {

        directionDroneLeft.x = OVRGamepadController.GPC_GetAxis(OVRGamepadController.Axis.LeftXAxis) ;
        directionDroneLeft.y = OVRGamepadController.GPC_GetAxis(OVRGamepadController.Axis.LeftYAxis) ;
        directionDroneLeft.z = 1f;


        directionDroneRight.x = OVRGamepadController.GPC_GetAxis(OVRGamepadController.Axis.RightXAxis);
        directionDroneRight.y = OVRGamepadController.GPC_GetAxis(OVRGamepadController.Axis.RightYAxis);
        directionDroneRight.z = 1f;

        if (OculusCurrentOrientation.IsDefined()) { 
            Quaternion headOrientation = OculusCurrentOrientation.GetOrientation();
            Vector3 eulerHeadOri = headOrientation.eulerAngles;
            Debug.Log("Euleur: " + eulerHeadOri);
        }

        playerTilt.z = 0f;
        playerTilt.z += OVRGamepadController.GPC_GetButton(OVRGamepadController.Button.Y)?-1f:0f;
        playerTilt.z += OVRGamepadController.GPC_GetButton(OVRGamepadController.Button.B)?1f:0f;

        playerDirection.x = 0f;
        playerDirection.x += OVRGamepadController.GPC_GetButton(OVRGamepadController.Button.LeftShoulder) ? -1f : 0f;
        playerDirection.x += OVRGamepadController.GPC_GetButton(OVRGamepadController.Button.RightShoulder) ? 1f : 0f;
        
        playerDirection.y = 0f;
        playerDirection.y += OVRGamepadController.GPC_GetButton(OVRGamepadController.Button.X) ? -0.5f : 0f;
        playerDirection.y += OVRGamepadController.GPC_GetButton(OVRGamepadController.Button.A) ? 0.5f : 0f;

        directionDroneLeftLerped = Vector3.Lerp(directionDroneLeftLerped, directionDroneLeft, Time.deltaTime * 2f);
        directionDroneRightLerped = Vector3.Lerp(directionDroneRightLerped, directionDroneRight, Time.deltaTime * 2f);

        playerTiltLerped = Vector3.Lerp(playerTiltLerped, playerTilt, Time.deltaTime );
        playerDirectionLerped = Vector3.Lerp(playerDirectionLerped, playerDirection, Time.deltaTime*2f);


        surferController.SetJoystickValue(0, directionDroneLeftLerped);
        surferController.SetJoystickValue(1, directionDroneRightLerped);
        surferController.SetJoystickValue(2, playerDirectionLerped);
        surferController.SetJoystickValue(3, playerTiltLerped);
    
    }

 

}
