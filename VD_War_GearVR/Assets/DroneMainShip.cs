﻿using UnityEngine;
using System.Collections;

public class DroneMainShip : MonoBehaviour
{

    public float speed = 1f;
    public GameObject playerPosition;
    public float playerDistanceWanted = 10f;

    public int collisionCount;
    public int life=50;
    public GameObject dronePrefab;
    public int toProduce=1;
    public float produceDelay=10f;
    public float apparitionRange=10f;
    void Start()
    {
        PopSetup();
        InvokeRepeating("PopDrones", 0, produceDelay);
    }

    void PopDrones() {

        for (int i = 0; i < toProduce; i++)
        {
            Vector3 where = transform.position;
            where += new Vector3(Random.Range(-apparitionRange, apparitionRange), -1, Random.Range(-apparitionRange, apparitionRange));
            GameObject newDrones = GameObject.Instantiate(dronePrefab, where, transform.rotation) as GameObject;   
        }
        toProduce++;


 
    }

    void Update()
    {

        //Vector3 direction = playerPosition.transform.position - transform.position;
        //if (direction.magnitude > playerDistanceWanted)
        //{
        //    transform.position += direction.normalized * (speed * Time.deltaTime);
        //}
       // direction.y = 0;
        //transform.rotation = Quaternion.LookRotation(direction.normalized);

    }

    public void OnCollisionEnter(Collision col)
    {

        collisionCount++;
        if(collisionCount>life){
            PopSetup();
            collisionCount = 0;

        }
        
    }

    public void PopSetup()
    {
        playerPosition = GameObject.FindWithTag("Player");
         //transform.position = new Vector3(Random.Range(-50, 50), 5, Random.Range(-50, 50));
         toProduce = (int)(1f + ((float)toProduce / 2f));

    }
}