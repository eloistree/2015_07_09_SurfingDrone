﻿using UnityEngine;
using System.Collections;

public class DroneKeyboardInterface : MonoBehaviour {

    public HandyDrone drone;
    public Transform origine;

    public KeyCode foward = KeyCode.A;
    public KeyCode back = KeyCode.Q;
    public KeyCode left = KeyCode.S;
    public KeyCode right = KeyCode.F;
    public KeyCode top = KeyCode.E;
    public KeyCode down = KeyCode.D;

	
	public void Update () {

        

        Vector3 dronePosition = drone.GetPosition();
        Vector3 dirFromOrigine = dronePosition - origine.position;

         Vector3  direction = Vector3.zero;
        if (Input.GetKey(foward))
            direction.z += 1f;
        else if(dirFromOrigine.z>0f)
            direction.z -= 1f;

        if (Input.GetKey(left))
            direction.x -= 1f;
        if (Input.GetKey(right))
            direction.x += 1f;
        if (Input.GetKey(top))
            direction.y += 1f;
        if (Input.GetKey(down))
            direction.y -= 1f;

            drone.SetAccelerate(direction == Vector3.zero);
            drone.SetDirection(direction); 
	



	}


}
