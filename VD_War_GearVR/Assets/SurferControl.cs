﻿using UnityEngine;
using System.Collections;

public class SurferControl : MonoBehaviour {

    public Vector3 handLeft = new Vector3(0, 0, 0.5f);
    public Vector3 handRight = new Vector3(0, 0, 0.5f);
    public Vector3 playerDirection;
    public Vector3 spineDirection = new Vector3(0, 1, 0);

    private Vector3 handLeftLerped = new Vector3(0, 0, 0.5f);
    private Vector3 handRightLerped = new Vector3(0, 0, 0.5f);
    private Vector3 playerDirectionLerped;
    private Vector3 spineDirectionLerped = new Vector3(0, 1,0f);


    public Transform playerRef;
    public Transform surfRef;
    //public Transform playerBodyPosRef;
    //public Transform playerBodyRootRef;
    //public float heightRatio=1f;

    public Vector3 playerMoveRatio=Vector3.one;
    public float surfMoveRatio=1.5f;
    public float angleSurfLateralRotation = 80f;
    public float angleSurfVertiacalRotation = 20f;


    public Transform droneZonePosition;
    public Vector3 droneMoveRatio = new Vector3(2f, 1f, 0.5f);

    public Transform shoulderLeftRef;
    public Transform handLeftPosition;
    public float handLeftDistance = 1.5f;
    public Transform droneLeftPosition;
    public float droneLeftDistance = 5f;

    public Transform shoulderRightRef;
    public Transform handRightPosition;
    public float handRightDistance = 1.5f;
    public Transform droneRightPosition;
    public float droneRightDistance = 5f;


    private Vector3 initPlayerPosition;
    private Vector3 initSurfPosition;

    void Start()
    {
        initPlayerPosition = playerRef.localPosition;
        initSurfPosition = surfRef.localPosition;

    }

	
	void Update () {

        LimitVector3(ref handLeft);
        LimitVector3(ref handRight);
        LimitVector3(ref playerDirection);
        handLeftLerped = Vector3.Lerp(handLeftLerped, handLeft,Time.deltaTime*4f);
        handRightLerped = Vector3.Lerp(handRightLerped, handRight, Time.deltaTime * 4f);
        playerDirectionLerped = Vector3.Lerp(playerDirectionLerped, playerDirection, Time.deltaTime * 3f);
        spineDirectionLerped = Vector3.Lerp(spineDirectionLerped, spineDirection, Time.deltaTime * 3f);

        handLeftPosition.position = shoulderLeftRef.position + handLeftLerped * handLeftDistance;
        handRightPosition.position = shoulderRightRef.position + handRightLerped * handRightDistance;


        Vector3 droneLeftPos = droneZonePosition.position + Multiply(handLeft, droneMoveRatio) * droneLeftDistance;
        Vector3 droneRightPos = droneZonePosition.position + Multiply(handRight, droneMoveRatio) * droneRightDistance;
        //if (handLeft.z <= 0.2f || handRight.z <= 0.2f){
        //    droneLeftPos = droneZonePosition.position;
        //    droneRightPos = droneZonePosition.position;
        //}
        droneLeftPos.x -= droneLeftDistance*0.6f +0.6f;
        droneRightPos.x += droneRightDistance * 0.6f + 0.6f;

        droneLeftPosition.position = Vector3.Lerp(droneLeftPosition.position, droneLeftPos, Time.deltaTime * 4f);
        droneRightPosition.position = Vector3.Lerp(droneRightPosition.position, droneRightPos, Time.deltaTime * 4f);



        Vector3 spineDir = spineDirectionLerped;
        spineDir.y = 0;

        Vector3 surfDirTmp = playerDirection;
        surfDirTmp.z = spineDir.z;
        Vector3 surfMoveTmp = playerDirection * 0.8f;
        
        Quaternion surfRot = Quaternion.identity;
        surfRot = Quaternion.Euler(new Vector3(0f, 0f, surfDirTmp.x * angleSurfLateralRotation)) * surfRot;
        surfRot = Quaternion.Euler(new Vector3(surfDirTmp.z * angleSurfVertiacalRotation, 0f, 0f)) * surfRot;

        surfMoveTmp.x *=1f;
       // surfMoveTmp.z = -Mathf.Abs(surfMoveTmp.y);
        surfMoveTmp.y = 0f;

        surfRef.localRotation = surfRot;
        surfRef.localPosition = initSurfPosition + surfMoveTmp * surfMoveRatio;

        Vector3 spineDirNorm = spineDirectionLerped.normalized;
        spineDirNorm.x = 0;
        float verticalTilt = Vector3.Angle(Vector3.up, spineDirNorm) * -Mathf.Sign(spineDirNorm.z);

        Vector3 playerMoveTmp = Vector3.zero;
        playerMoveTmp.x = playerDirection.x;
        playerMoveTmp.z = playerDirection.z;
        playerMoveTmp.y = verticalTilt /90f;

        playerRef.localPosition = initPlayerPosition + Multiply( playerMoveTmp , playerMoveRatio);

        handLeftPosition.rotation = Quaternion.LookRotation(droneLeftPosition.position);
        handRightPosition.rotation = Quaternion.LookRotation(droneRightPosition.position);

        //Vector3 playerPosOnSurf = playerBodyRootRef.position;
        //playerPosOnSurf.y += heightRatio * playerDirection.y;
        //playerBodyPosRef.position = playerPosOnSurf;
	
	}

    private Vector3 Multiply(Vector3 handLeft, Vector3 droneMoveRatio)
    {
        handLeft.x *= droneMoveRatio.x;
        handLeft.y *= droneMoveRatio.y;
        handLeft.z *= droneMoveRatio.z;
        return handLeft;
    }

    private void LimitVector3(ref Vector3 value)
    {
        value.x = Mathf.Clamp(value.x, -1f, 1f);
        value.y = Mathf.Clamp(value.y, -1f, 1f);
        value.z = Mathf.Clamp(value.z, -1f, 1f);
    }

    public void SetPlayerDirection(Vector3 newValue)
    {
        playerDirection = newValue;
    }

    public void SetHandRightDirection(Vector3 newValue)
    {
        handRight = newValue;
    }

    public void SetHandLeftDirection(Vector3 newValue)
    {
        handLeft = newValue;
    }

    internal Vector3 GetPlayerDirection(bool lerped)
    {
        if (lerped) return playerDirectionLerped;
        else return playerDirection;
    }

    internal void SetPlayerSpineDirection(Vector3 newValue)
    {
        spineDirection = newValue;
    }

    internal Vector3 GetSpineDirection( bool lerped)
    {    
        if (lerped) return spineDirectionLerped;
        else return spineDirection;
    
    }

    internal Transform GetDroneLeft()
    {
        return droneLeftPosition;
    }

    internal Transform GetDroneRight()
    {
        return droneRightPosition;
    }

    internal Vector3 GetHandLeft()
    {
        return handLeft;
    }

    internal Vector3 GetHandRight()
    {

        return handRight;
    }
}
