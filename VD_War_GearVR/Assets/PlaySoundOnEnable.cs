﻿using UnityEngine;
using System.Collections;

public class PlaySoundOnEnable : MonoBehaviour {

    public AudioSource audioSource;
    void OnEnable() {
        audioSource.Play();
    }
}
