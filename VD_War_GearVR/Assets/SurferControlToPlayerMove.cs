﻿using UnityEngine;
using System.Collections;

public class SurferControlToPlayerMove : MonoBehaviour {

    public SurferControl surferControl;
    public Transform playerRootRef;

    public float maxRadius= 40f;
    public Vector3 actualPosition;
    public float lateralSpeed=10f;
    public float verticalSpeed=3f;

    public Vector3 maxBorder;
	void Update () {

        Vector3 playerDirection = surferControl.GetPlayerDirection(true);
        Vector3 playerPosition =   playerRootRef.localPosition;
        playerPosition.z = 0;

        playerDirection.x *= lateralSpeed  ;
        playerDirection.y *= verticalSpeed ;
        playerPosition += playerDirection*Time.deltaTime;

        if (playerPosition.magnitude >= maxRadius) {
            //Vector3 oppDirection = playerDirection.normalized;
            //oppDirection.x *= -lateralSpeed*1.1f;
            //oppDirection.y *= -verticalSpeed * 1.1f;
            ////if (Mathf.Abs(playerPosition.x) < maxRadius)
            ////    oppDirection.x = 0f;
            ////if (Mathf.Abs(playerPosition.y) < maxRadius)
            ////    oppDirection.y = 0f;
            //playerPosition += oppDirection * Time.deltaTime;
            return;
        }

        playerRootRef.localPosition = playerPosition;

	}
}

        //maxBorder = new Vector3(maxRadius, maxRadius, maxRadius);

        //if (playerPosition != Vector3.zero)
        //{
        //    float playerPctFromMax = Mathf.Clamp(playerPosition.magnitude / maxRadius, 0f, 1f);
        //    maxBorder = playerPosition /= playerPctFromMax;
        //}

        //if (Mathf.Abs(playerPosition.x) > Mathf.Abs(maxBorder.x))
        //    playerPosition.x = maxBorder.x+0.02f *Mathf.Sign(maxBorder.x);

        //if (Mathf.Abs(playerPosition.y) > Mathf.Abs(maxBorder.y))
        //    playerPosition.y = maxBorder.y + 0.02f * Mathf.Sign(maxBorder.y);
        