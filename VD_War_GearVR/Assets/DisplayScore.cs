﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DisplayScore : MonoBehaviour {

    public Text score;
    public Text bestScore;

    
    void Start () {
        CointScore.SaveScore();
        score.text = "" + CointScore.score;
	  bestScore.text =""+  CointScore.GetBestScore();
      CointScore.ResetScore();
	}
	
}
