﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SurfLine : MonoBehaviour {

    public LineRenderer lineRenderer;
    public Transform startLine;
    public Transform startRecord;
    public Queue<Vector3> record = new Queue<Vector3>();
    public int maxRecord=50;
    public float timeBetweenRecord=0.1f;
    private float recordCount;
    public float distanceBetweenRecord = 0.5f;

    void Start() {
    
    }

    void Update()
    {
        recordCount -= Time.deltaTime;
        if (recordCount <= 0) {
            recordCount = timeBetweenRecord;

            record.Enqueue(startRecord.position);
            if(record.Count>=maxRecord) 
                record.Dequeue();


            lineRenderer.SetVertexCount(record.Count + 2);
        }


        lineRenderer.SetPosition(0, startLine.position);
        lineRenderer.SetPosition(1, startRecord.position);
        Vector3[] rec = record.ToArray();
        Vector3 pos;
        float distToMove = 0;
        for (int i =0; i <rec.Length; i++)
        {
            distToMove+=distanceBetweenRecord;
            pos = rec[rec.Length-1-i];
            pos.z -= distToMove;
            lineRenderer.SetPosition(i+2,pos);
        }


	
	}
}
