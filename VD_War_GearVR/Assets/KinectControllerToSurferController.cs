﻿using UnityEngine;
using System.Collections;

public class KinectControllerToSurferController : MonoBehaviour {


    public string surferControllerName= "SurferInput";
    public string kinectControllerName = "KinectInfo";
    public VirtualController surferController;
    public VirtualController kinectController;
	// Use this for initialization
    void Start()
    {
        kinectController = VirtualController.Get(kinectControllerName);
        surferController = VirtualController.Get(surferControllerName);

        kinectController.GetPosition(0).onValueChanged += SetLeftHand;
        kinectController.GetPosition(1).onValueChanged += SetRightHand;
        kinectController.GetPosition(2).onValueChanged += SetPlayerDirection;
        kinectController.GetPosition(3).onValueChanged += SetPlayerSpineDirection;
	}

    private void SetPlayerDirection(int index, Vector3 oldValue, Vector3 newValue)
    {
        //newValue.y = -newValue.z;
        //newValue.z = 0f;
        //if (newValue.y < 0) newValue.y *= 2f;

        surferController.SetJoystickValue(2, newValue.normalized);
    }
    private void SetPlayerSpineDirection(int index, Vector3 oldValue, Vector3 newValue)
    {
        surferController.SetJoystickValue(3, newValue.normalized );
    }

    private void SetRightHand(int index, Vector3 oldValue, Vector3 newValue)
    {
        surferController.SetJoystickValue(1, newValue );
    }

    private void SetLeftHand(int index, Vector3 oldValue, Vector3 newValue)
    {
        surferController.SetJoystickValue(0, newValue );
    }

}
