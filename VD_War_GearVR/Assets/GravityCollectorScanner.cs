﻿using UnityEngine;
using System.Collections;

public class GravityCollectorScanner : MonoBehaviour {

    public GravityCollector gravityEngine;
    public LayerMask allowLayers;
    public float detectionRadius = 5f;
    public float timeBetweenCheck = 0.25f;

   

    public void OnEnable()
    {
        InvokeRepeating("Scan", 0, timeBetweenCheck);

    }
    public void Scan()
    {
        Collider[] cols = Physics.OverlapSphere(gameObject.transform.position, detectionRadius, allowLayers);
        foreach (Collider col in cols)
            if (col != null)
            {
                gravityEngine.AddCollectableObject(col.gameObject);
            }


    }
}
